# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the importexample.cibersheep package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: importexample.cibersheep\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-12-16 00:25+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/PageImport.qml:15
msgid "Import a file from"
msgstr ""

#: ../qml/PageImport.qml:19
msgid "Back"
msgstr ""

#: ../qml/Main.qml:40 importexample.desktop.in.h:1
msgid "Import Example"
msgstr ""

#: ../qml/Main.qml:45
msgid "Import a file"
msgstr ""
